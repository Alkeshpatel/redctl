// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"gitlab.com/redfield/redctl/api"
)

// ImageRepo interface
type ImageRepo interface {
	// Create new RAW or QCOW2 image
	Create(name string, bytes int64) (*api.Image, error)

	// CreateBacked QCOW2 image backed with another image (raw|qcow2)
	// If bytes is zero, the size will match backing image
	CreateBacked(name string, backingImageName string, bytes int64) (*api.Image, error)

	// Copy one image to another, and return the destination image after
	// successful creation
	Copy(source string, destination string) (*api.Image, error)

	// Find image
	Find(name string) (*api.Image, error)

	// Find all images
	FindAll() ([]*api.Image, error)

	// Remove image
	Remove(name string) error
}
