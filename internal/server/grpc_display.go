// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/redfield/redctl/api"
)

const (
	etcPath  = "/storage/etc"
	xorgPath = "/storage/etc/xorg.conf"
)

func getPrimaryDisplay() (string, error) {
	out, err := exec.Command("xrandr").Output()
	if err != nil {
		log.Println("failed to exec xandr:", err)
		return "", errors.New("failed to exec xrandr")
	}

	//Screen 0: minimum 320 x 200, current 1920 x 1200, maximum 8192 x 8192
	//HDMI-1 disconnected primary (normal left inverted right x axis y axis)
	//DP-1 connected 1920x1200+0+0 (normal left inverted right x axis y axis) 518mm x 324mm
	//   1920x1200     59.95*+
	//   1920x1080     60.00
	//   1600x1200     60.00
	//   1680x1050     59.95
	//   1280x1024     60.02
	//   1280x960      60.00
	//   1024x768      60.00
	//   800x600       60.32
	//   640x480       59.94
	//   720x400       70.08
	// HDMI-2 disconnected (normal left inverted right x axis y axis)

	osplit := strings.Split(string(out), "\n")

	display := ""
	for _, line := range osplit {
		log.Println("processing:", line)
		if strings.Contains(line, " connected") {
			lsplit := strings.Split(line, " ")
			log.Println("lsplit:", lsplit)
			if len(lsplit) < 1 {
				break
			}
			display = lsplit[0]
		}
	}

	if display == "" {
		log.Println("failed to parse xandr:", osplit)
		return "", errors.New("failed to parse xrandr")
	}

	log.Printf("The primary display is: %s\n", display)
	return display, nil
}

func setDisplayMode(display string, mode string) error {
	log.Println("setting display", display, "to mode", mode)
	out, err := exec.Command("xrandr", "--output", display, "--mode", mode).CombinedOutput()
	if err != nil {
		log.Println("failed to set resolution with xandr:", out)
		return errors.New("resolution not supported")
	}

	log.Println("mode set to", mode, "for display", display)
	return nil
}

func setXorgConf(mode string) error {
	// if mode is empty, clear it
	if mode == "" {
		// if no xorg config exists, do nothing
		if _, err := os.Stat(xorgPath); os.IsNotExist(err) {
			return nil
		}

		// if xorg config exists, delete it
		err := os.Remove(xorgPath)
		if err != nil {
			return err
		}

		log.Println("xorg cleared")
		return nil
	}

	xorg := []byte(`
Section "Screen"
	Identifier "Default Screen"
	SubSection "Display"
		Modes "` + mode + `"
	EndSubSection
EndSection
`)

	if err := os.MkdirAll(etcPath, 0755); err != nil {
		return errors.New("failed to create /storage/etc")
	}

	if err := ioutil.WriteFile(xorgPath, xorg, 0644); err != nil {
		return errors.New("failed to write /storage/etc/xorg.conf")
	}

	return nil
}

func (s *server) DisplayForceResolution(ctx context.Context, in *api.DisplayForceResolutionRequest) (*api.DisplayForceResolutionReply, error) {
	mode := in.GetMode()

	if mode != "" {
		display, err := getPrimaryDisplay()
		if err != nil {
			return nil, err
		}

		err = setDisplayMode(display, mode)
		if err != nil {
			return nil, err
		}
	}

	err := setXorgConf(mode)
	if err != nil {
		return nil, err
	}

	return &api.DisplayForceResolutionReply{}, nil
}
