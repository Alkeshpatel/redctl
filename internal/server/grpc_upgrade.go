// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"

	"gitlab.com/redfield/redctl/api"
)

func upgradeControlDomain(imagePath string) error {
	if imagePath == "" {
		return fmt.Errorf("empty image path is invalid")
	}

	_, err := os.Stat(imagePath)
	if err != nil {
		return fmt.Errorf("cannot perform upgrade, no new control domain found")
	}

	err = exec.Command("installer", "-upgrade-minimal", imagePath).Run()
	if err != nil {
		return fmt.Errorf("failed to perform control domain upgrade")
	}

	return nil
}

func (s *server) UpgradeControlDomain(ctx context.Context, in *api.UpgradeControlDomainRequest) (*api.UpgradeControlDomainReply, error) {
	name := in.GetName()

	image, err := s.imageRepo.Find(name)
	if err != nil {
		log.Printf("Failed to locate control domain image: %v %v\n", name, err)
		return nil, fmt.Errorf("failed to locate control domain image: %v %v", name, err)
	}

	err = upgradeControlDomain(image.GetPath())
	if err != nil {
		log.Printf("Failed to perform control domain upgrade from path %v: %v\n", image.GetPath(), err)
		return nil, fmt.Errorf("failed to perform control domain upgrade from path %v: %v", image.GetPath(), err)
	}

	return &api.UpgradeControlDomainReply{}, nil
}
