// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ui

import (
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"path/filepath"
	"strings"
)

const desktopEntryTemplate = `
[Desktop Entry]
Name=%v
Icon=%v
Exec=redctl vm start --name %v
Terminal=false
Type=Application
StartupNotify=true
StartupWMClass=%v
`

func (s *Session) createDesktopEntry(name, icon string) error {
	path := filepath.Join(s.appsDir, fmt.Sprintf("redfield-%v.desktop", name))

	iconPath, err := s.client.GraphicFind(icon)
	if err != nil {
		return err
	}

	entry := fmt.Sprintf(desktopEntryTemplate, name, iconPath, name, name)

	return ioutil.WriteFile(path, []byte(entry), 0644)
}

func (s *Session) populateDock() error {
	names := make([]string, 0)

	domains, err := s.client.DomainFindAll()
	if err != nil {
		return err
	}

	for _, domain := range domains {
		// If a guest has an icon set, create a desktop entry.
		icon := domain.GetDesktopIcon()
		if icon == "" {
			continue
		}

		name := domain.GetConfig().GetName()

		err := s.createDesktopEntry(name, icon)
		if err != nil {
			// Don't return - still try to create the other icons
			log.Printf("error: unable to create desktop icon for domain %v: %v", name, err)
		}
		names = append(names, fmt.Sprintf("'redfield-%v.desktop'", name))
	}

	return dconfWriteFavorites(names)
}

func dconfWriteFavorites(favorites []string) error {
	v := fmt.Sprintf("[%v]", strings.Join(favorites, ","))

	cmd := exec.Command("dconf", "write", "/org/gnome/shell/favorite-apps", v)
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Print(string(out))
		return err
	}

	return nil
}
