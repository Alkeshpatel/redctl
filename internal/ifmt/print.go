package ifmt

import (
	"encoding/json"
	"fmt"
)

// Prettify takes an object and prettifies it
// Falls back to Sprintf if unable to prettify
// Returns prettified string.
func Prettify(v interface{}) (val string) {
	b, err := json.MarshalIndent(v, "", "    ")
	if err == nil {
		return string(b)
	}

	return fmt.Sprintf("%+v", v)
}

// PrettyPrint takes an object, prettifies, and prints it to stdout.
func PrettyPrint(v interface{}) (err error) {
	b, err := json.MarshalIndent(v, "", "    ")
	if err == nil {
		fmt.Println(string(b))
	}
	return
}
