#!/usr/bin/env bats

REDCTL_ROOT_PATH="/tmp/redctl-test"
REDCTL_IMAGES_PATH="$REDCTL_ROOT_PATH/storage/images"
REDCTL_SOCKET_PATH="unix://$REDCTL_ROOT_PATH/redctld.socket"
REDCTL_CMD="redctl --server-url $REDCTL_SOCKET_PATH --server-timeout 5s"
REDCTLD_CMD="redctld --server-root-path $REDCTL_ROOT_PATH --server-url $REDCTL_SOCKET_PATH"
PATH=${PATH}:bin

setup() {
	mkdir -p $REDCTL_IMAGES_PATH
	$REDCTLD_CMD &
	echo $PATH
	sleep 1
	REDCTLD_PID=$!
}

teardown() {
	killall redctld
	for i in $(seq 1 5); do
		if ! pidof redctld; then
			break
		fi

		sleep 1
	done

	if pidof redctld; then
		echo "force killing redctld" >&2
		killall -9 redctld
	fi

	rm -rf $REDCTL_ROOT_PATH
}

@test "image create" {
	run $REDCTL_CMD image create --name test1.qcow2
        [ $status -eq 0 ]
	[ -f $REDCTL_IMAGES_PATH/test1.qcow2 ]
}

@test "image remove" {
	$REDCTL_CMD image create --name test1.qcow2
        run $REDCTL_CMD image remove --name test1.qcow2
        [ $status -eq 0 ]
        [ ! -f $REDCTL_IMAGES_PATH/test1.qcow2 ]
}

@test "image copy" {
	$REDCTL_CMD image create --name test1.qcow2
        run $REDCTL_CMD image copy --source test1.qcow2 --destination test2.qcow2
        [ $status -eq 0 ]
        [ -f $REDCTL_IMAGES_PATH/test2.qcow2 ]
}

@test "image copy (invalid source)" {
        run $REDCTL_CMD image copy --source test1.qcow2 --destination test2.qcow2
        [ $status -ne 0 ]
        [ ! -f $REDCTL_IMAGES_PATH/test2.qcow2 ]
}

@test "vm create" {
        run $REDCTL_CMD vm create --name test1
	[ $status -eq 0 ]
}

@test "pci add (w/o vm name)" {
        run $REDCTL_CMD vm pci assign --address 0000:00:1f.0
	[ $status -ne 0 ]
}

@test "pci add" {
        $REDCTL_CMD vm create --name test1
        run $REDCTL_CMD vm pci --name test1 assign --address 0000:00:1f.0
	[ $status -eq 0 ]
}

@test "pci list" {
        $REDCTL_CMD vm create --name test1
        $REDCTL_CMD vm --name test1 pci assign --address 0000:00:1f.0
        run $REDCTL_CMD vm --name test1 pci list
	[ $status -eq 0 ]
	# TODO: verify output
}

@test "pci remove (by address)" {
        $REDCTL_CMD vm create --name test1
        $REDCTL_CMD vm --name test1 pci assign --address 0000:00:1f.0
        run $REDCTL_CMD vm --name test1 pci remove --address 0000:00:1f.0
	[ $status -eq 0 ]
}

@test "pci remove (by index)" {
        $REDCTL_CMD vm create --name test1
        $REDCTL_CMD vm --name test1 pci assign --address 0000:00:1f.0
        run $REDCTL_CMD vm --name test1 pci remove --index 0
        [ $status -eq 0 ]
}

@test "pci remove (invalid index)" {
        $REDCTL_CMD vm create --name test1
        run $REDCTL_CMD vm --name test1 pci remove --index 2
        [ $status -ne 0 ]
}
